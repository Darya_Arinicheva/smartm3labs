import sofia_kp.KPICore;
import sofia_kp.iKPIC_subscribeHandler;
import sofia_kp.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SS_Agent implements iKPIC_subscribeHandler {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getAnonymousLogger();
    //private static Logger log = Logger.getLogger(SS_Agent.class.getName());

    public SS_Agent(String HOST, int PORT, String SMART_SPACE_NAME) {

        log.setLevel(Level.FINER);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINER);
        log.addHandler(handler);
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }

    public String decreaseMoney(String subject, String predicate, String object) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.queryRDF(subject, "http://workers.ru/RDF/money", null, "uri", "literal");
            String[] duration = object.split("-");
            long time = Long.valueOf(duration[1]) - Long.valueOf(duration[0]);
            long days = TimeUnit.MILLISECONDS.toDays(time);
            Vector<Vector<String>> triple = kpi.xmlTools.getQueryTriple(retXml);
            long money = Long.valueOf(triple.get(0).get(2));
            money -= days * 0.5;
            if (predicate.equals("http://workers.ru/RDF/holiday_no_money")) {
                money -= days * 0.5;
            }
            retXml = kpi.insert(subject,    // subject
                    "http://workers.ru/RDF/money",  // predicate
                    String.valueOf(money),     // object
                    "uri",      // subject type
                    "literal");   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String remove(String subject, String predicate, String object, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.remove(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isRemoveConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Remove of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Remove of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    @Override
    public void kpic_SIBEventHandler(String s) {
        synchronized (kpi) {
            Vector<Vector<String>> deleted = kpi.xmlTools.getObsoleteResultEventTriple(s);
            Vector<Vector<String>> inserted = kpi.xmlTools.getNewResultEventTriple(s);
            String id = kpi.xmlTools.getSubscriptionID(s);
            if (!inserted.isEmpty()) {
                System.out.println("subject: " + inserted.get(0).get(0) + " predicate: " + inserted.get(0).get(1) + " object: " + inserted.get(0).get(2));
                try {
                    long endDate = Long.valueOf(inserted.get(0).get(2).split("-")[1]);
                    if (endDate > System.currentTimeMillis()) {
                        remove(inserted.get(0).get(0), "http://workers.ru/RDF/status", null, "literal");
                        insert(inserted.get(0).get(0), "http://workers.ru/RDF/status", inserted.get(0).get(1), "uri", "literal");
                    }
                    switch (inserted.get(0).get(1)) {
                        case "http://workers.ru/RDF/holiday" :
                        case "http://workers.ru/RDF/illness" :
                        case "http://workers.ru/RDF/mission" :
                        case "http://workers.ru/RDF/holiday_no_money" :
                            decreaseMoney(inserted.get(0).get(0), inserted.get(0).get(1), inserted.get(0).get(2));
                            break;
                    }
                } catch (Exception e) {}
            }
        }
    }

    public void subscribe(String subject, String predicate, String object, String objectType) {
        synchronized (kpi) {

            String retXml = kpi.subscribeRDF(subject, predicate, object, objectType);
            if (retXml != null && kpi.xmlTools.isSubscriptionConfirmed(retXml)) {
                // ОБРАТИТЕ ВНИМАНИЕ, КАК ПРАВИЛЬНО ОПРЕДЕЛЯЕТСЯ SUBSCRIPTION_ID
                String subscriptionId = kpi.xmlTools.getSubscriptionID(retXml);
                if (subscriptionId != null && !subscriptionId.isEmpty()) {
                    subscriptionIdList.add(kpi.xmlTools.getSubscriptionID(retXml));
                }
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> confirmed ", new Date(), predicate));
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> was aborted ", new Date(), predicate));
                }
            }
        }
    }

    public void unSubscribeAll() {
        String retXml;
        synchronized (kpi) {
            for (String id : subscriptionIdList) {
                retXml = kpi.unsubscribe(id);
                if ((retXml != null && retXml.equals(""))) {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription is confirmed.", new Date()));
                    }
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription isn't confirmed", new Date()));
                    }
                }
            }
        }
        subscriptionIdList = new ArrayList<String>();
    }

    public void start() {
        String retXml = kpi.join();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully joined to smart space.");
        } else {
            log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            return;
        }
        kpi.setEventHandler(this);
    }

    public void stop() {
        // Cancel all the subscriptions
        unSubscribeAll();

        // Leave smart space
        String retXml = kpi.leave();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    public List<String> getSubscriptionIdList() {
        return subscriptionIdList;
    }

    public void setSubscriptionIdList(List<String> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;

        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public static void main(String[] args) throws SmartSpaceException {

        SS_Agent service = new SS_Agent("127.0.0.1", 10010, "X");
        service.start();
        log.info("Started");
        service.remove(null, "http://workers.ru/RDF/money", null, "literal");
        service.remove(null, "http://workers.ru/RDF/status", null, "literal");
        service.insert("bob@gmail.com", "http://workers.ru/RDF/status", "works", "uri", "literal");
        service.insert("bob@gmail.com", "http://workers.ru/RDF/money", "100", "uri", "literal");
        service.insert("tom@gmail.com", "http://workers.ru/RDF/status", "works", "uri", "literal");
        service.insert("tom@gmail.com", "http://workers.ru/RDF/money", "1000", "uri", "literal");

        service.subscribe(null, "http://workers.ru/RDF/works", null, "literal");
        service.subscribe(null, "http://workers.ru/RDF/holiday", null, "literal");
        service.subscribe(null, "http://workers.ru/RDF/holiday_no_money", null, "literal");
        service.subscribe(null, "http://workers.ru/RDF/illness", null, "literal");
        service.subscribe(null, "http://workers.ru/RDF/mission", null, "literal");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Type 'quit <Enter>' to stop service and leave.\n> ");
        try {
            String cmd = bufferedReader.readLine();
            while (true) {
                if (cmd.equals("exit") || cmd.equals("quit") || cmd.equals("stop")) {
                    break;
                }
                if (!cmd.isEmpty()) {
                    System.out.println(String.format("Unknown command: '%s'", cmd));
                }
                System.out.print("> ");
                cmd = bufferedReader.readLine();
            }
        } catch (IOException ex) {
            System.out.println("Can't read next command. Stopping...");
        } finally {
            service.stop();
        }
        log.log(Level.INFO, "Bye.");
    }
}
