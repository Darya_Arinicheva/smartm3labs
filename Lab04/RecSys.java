import sofia_kp.KPICore;
import sofia_kp.iKPIC_subscribeHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RecSys implements iKPIC_subscribeHandler {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getAnonymousLogger();
    //private static Logger log = Logger.getLogger(SS_Agent.class.getName());

    public RecSys(String HOST, int PORT, String SMART_SPACE_NAME) {
        log.setLevel(Level.FINER);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINER);
        log.addHandler(handler);
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }

    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String remove(String subject, String predicate, String object, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.remove(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isRemoveConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Remove of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Remove of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    @Override
    public void kpic_SIBEventHandler(String s) {
        synchronized (kpi) {
            Vector<Vector<String>> deleted = kpi.xmlTools.getObsoleteResultEventTriple(s);
            Vector<Vector<String>> inserted = kpi.xmlTools.getNewResultEventTriple(s);
            String id = kpi.xmlTools.getSubscriptionID(s);
            if (!inserted.isEmpty()) {
                System.out.println("subject: " + inserted.get(0).get(0) + " predicate: " + inserted.get(0).get(1) + " object: " + inserted.get(0).get(2));
            }
        }
    }

    public void subscribe(String subject, String predicate, String object, String objectType) {
        synchronized (kpi) {

            String retXml = kpi.subscribeRDF(subject, predicate, object, objectType);
            if (retXml != null && kpi.xmlTools.isSubscriptionConfirmed(retXml)) {
// ОБРАТИТЕ ВНИМАНИЕ, КАК ПРАВИЛЬНО ОПРЕДЕЛЯЕТСЯ SUBSCRIPTION_ID
                String subscriptionId = kpi.xmlTools.getSubscriptionID(retXml);
                if (subscriptionId != null && !subscriptionId.isEmpty()) {
                    subscriptionIdList.add(kpi.xmlTools.getSubscriptionID(retXml));
                }
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> confirmed ", new Date(), predicate));
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> was aborted ", new Date(), predicate));
                }
            }

        }
    }

    public void unSubscribeAll() {
        String retXml;
        synchronized (kpi) {
            for (String id : subscriptionIdList) {
                retXml = kpi.unsubscribe(id);
                if ((retXml != null && retXml.equals(""))) {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription is confirmed.", new Date()));
                    }
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription isn't confirmed", new Date()));
                    }
                }
            }
        }
        subscriptionIdList = new ArrayList<String>();
    }

    public void start() {
        String retXml = kpi.join();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully joined to smart space.");
        } else {
            log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            return;
        }
        kpi.setEventHandler(this);
    }

    public void stop() {
        // Cancel all the subscriptions
        unSubscribeAll();

        // Leave smart space
        String retXml = kpi.leave();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    public List<String> getSubscriptionIdList() {
        return subscriptionIdList;
    }

    public void setSubscriptionIdList(List<String> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;

        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public static Double[][] data = {
            {4.0, 3.0, null, null},
            {null, 4.0, null, 1.0},
            {null, null, 3.0, 4.0},
            {2.0, 4.0, null, null}
    };

    public static int size = 4;

    public static Double[] avgScore = new Double[size];

    public static void calcAvgScore() {
        for (int i = 0; i < size; i++) {
            double sum = 0;
            double count = 0;
            for (int j = 0; j < size; j++) {
                if (data[j][i] != null) {
                    sum += data[j][i];
                    count++;
                }
            }
            avgScore[i] = sum / count;
        }
    }

    public static double calcCorr(int user, int otherUser) {
        double userAvg = 0;
        double otherUserAvg = 0;
        double count = 0;
        for (int i = 0; i < size; i++) {
            if (data[i][user] != null && data[i][otherUser] != null) {
                userAvg += data[i][user];
                otherUserAvg += data[i][otherUser];
                count += 1;
            }
        }
        userAvg /= count;
        otherUserAvg /= count;
        double sum1 = 0;
        double sum2 = 0;
        double sum3 = 0;
        for (int i = 0; i < size; i++) {
            if (data[i][user] != null && data[i][otherUser] != null) {
                sum1 += (data[i][user] - userAvg) * (data[i][otherUser] - otherUserAvg);
                sum2 += Math.pow(data[i][user] - userAvg, 2);
                sum3 += Math.pow(data[i][otherUser] - otherUserAvg, 2);
            }
        }
        return (sum2 * sum3 > 0.001) ? (sum1 / Math.sqrt(sum2 * sum3)) : 0;
    }

    public static double calcScore(int user, int item) {
        double sum1 = 0;
        double sum2 = 0;
        for (int i = 0; i < size; i++) {
            if (data[item][i] != null) {
                double corr = calcCorr(user, i);
                sum1 += (data[item][i] - avgScore[i]) * corr;
                sum2 += Math.abs(corr);
            }
        }
        return avgScore[user] + ((sum2 > 0.001) ? (sum1 / sum2) : 0);
    }

    public static void main(String[] args) throws SS_Agent.SmartSpaceException {
        SS_Agent service = new SS_Agent("127.0.0.1", 10010, "X");
        service.start();
        calcAvgScore();
        log.info("Started");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (data[i][j] == null) {
                    double score = calcScore(j, i);
                    service.insert("user " + String.valueOf(j), "http://cais.iias.spb.su/RDF/score " + String.valueOf(score), "item " + i, "uri", "literal");
                }
            }
        }
        log.log(Level.INFO, "Bye.");
    }
}