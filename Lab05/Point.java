import java.util.Random;

public class Point {
    int id;
    int previousPoint;
    int driveByVehicle;
    int latitude;
    int longtitude;
    int date;
    int time;
    int vacantSeats;
    int vacantItemPlace;
    int waitTime;

    public Point() {
        Random random = new Random(31);
        id = random.nextInt();
        previousPoint = -1;
        driveByVehicle = -1;
        latitude = random.nextInt(90);
        longtitude = random.nextInt(180);
        date = random.nextInt();
        time = random.nextInt();
        vacantSeats = 10;
        vacantItemPlace = 10;
        waitTime = 5;
    }
}