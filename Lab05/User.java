public class User {
    int id;
    String name;
    Point[] points;
    int delay;

    public User(int id) {
        this.id = id;
        name = "name" + id;
        points = new Point[10];
        points[0] = new Point();
        delay = 10;
    }
}
