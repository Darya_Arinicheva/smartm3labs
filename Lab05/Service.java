import sofia_kp.KPICore;
import sofia_kp.iKPIC_subscribeHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Service implements iKPIC_subscribeHandler {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getAnonymousLogger();
    //private static Logger log = Logger.getLogger(SS_Agent.class.getName());

    public Service(String HOST, int PORT, String SMART_SPACE_NAME) {
        log.setLevel(Level.FINER);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINER);
        log.addHandler(handler);
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }

    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType).toString();   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String remove(String subject, String predicate, String object, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.remove(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType).toString();   // object type
            if (retXml != null && kpi.xmlTools.isRemoveConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Remove of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Remove of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    @Override
    public void kpic_SIBEventHandler(String s) {
        synchronized (kpi) {
            Vector<Vector<String>> deleted = kpi.xmlTools.getObsoleteResultEventTriple(s);
            Vector<Vector<String>> inserted = kpi.xmlTools.getNewResultEventTriple(s);
            String id = kpi.xmlTools.getSubscriptionID(s);
            if (!inserted.isEmpty()) {
                System.out.println("subject: " + inserted.get(0).get(0) + " predicate: " + inserted.get(0).get(1) + " object: " + inserted.get(0).get(2));
            }
        }
    }

    public void subscribe(String subject, String predicate, String object, String objectType) {
        synchronized (kpi) {
            String retXml = kpi.subscribeRDF(subject, predicate, object, objectType).toString();
            if (retXml != null && kpi.xmlTools.isSubscriptionConfirmed(retXml)) {
                // ОБРАТИТЕ ВНИМАНИЕ, КАК ПРАВИЛЬНО ОПРЕДЕЛЯЕТСЯ SUBSCRIPTION_ID
                String subscriptionId = kpi.xmlTools.getSubscriptionID(retXml);
                if (subscriptionId != null && !subscriptionId.isEmpty()) {
                    subscriptionIdList.add(kpi.xmlTools.getSubscriptionID(retXml));
                }
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> confirmed ", new Date(), predicate));
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> was aborted ", new Date(), predicate));
                }
            }
        }
    }

    public void unSubscribeAll() {
        String retXml;
        synchronized (kpi) {
            for (String id : subscriptionIdList) {
                retXml = kpi.unsubscribe(id).toString();
                if ((retXml != null && retXml.equals(""))) {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription is confirmed.", new Date()));
                    }
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription isn't confirmed", new Date()));
                    }
                }
            }
        }
        subscriptionIdList = new ArrayList<String>();
    }

    public void start() {
        String retXml = kpi.join().toString();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully joined to smart space.");
        } else {
            log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            return;
        }
        kpi.setEventHandler(this);
    }

    public void stop() {
        // Cancel all the subscriptions
        unSubscribeAll();

        // Leave smart space
        String retXml = kpi.leave().toString();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    public List<String> getSubscriptionIdList() {
        return subscriptionIdList;
    }

    public void setSubscriptionIdList(List<String> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;

        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public void insertDriversToSmartSpace(List<Driver> drivers) throws SmartSpaceException {
        for (Driver cur : drivers) {
            this.insert("id-" + cur.id, "http://users.ru/RDF/userType", "driver", "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/vehicleType", cur.vehicleType, "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/detour", String.valueOf(cur.detour), "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/name", String.valueOf(cur.name), "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/delay", String.valueOf(cur.delay), "uri", "literal");
        }
    }

    public void insertPassengersToSmartSpace(List<Passenger> passengers) throws SmartSpaceException {
        for (Passenger cur : passengers) {
            this.insert("id-" + cur.id, "http://users.ru/RDF/userType", "passenger", "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/detour", String.valueOf(cur.detour), "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/name", String.valueOf(cur.name), "uri", "literal");
            this.insert("id-" + cur.id, "http://users.ru/RDF/delay", String.valueOf(cur.delay), "uri", "literal");
        }
    }

    public void getUserInfo(int id) {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.queryRDF("id-" + String.valueOf(id), null, null, "uri", "literal").toString();
            Vector<Vector<String>> triple = kpi.xmlTools.getQueryTriple(retXml);
            String ans = "";
            for (Vector<String> v : triple) {
                ans = "";
                for (String s : v) {
                    ans += s + " ";
                }
                ans.replace('\n', ' ');
                System.out.println(ans);
            }
            if (ans.equals("")) {
                System.out.println("User not found");
            }
        }
    }

    public static void main(String[] args) throws SmartSpaceException, InterruptedException {

        Service service = new Service("127.0.0.1", 10010, "X");
        service.start();
        log.info("Started");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        Thread.currentThread().sleep(20);
        //service.remove(null, null, null, "literal");
        System.out.flush();
        System.out.println("Type number of drivers and passengers to generate. Type \"quit\" to exit the app.");
        System.out.println("Type \"user <ID>\" to view information about user.\n");
        try {
            while (true) {
                try {
                    System.out.print("> ");
                    String cmd = bufferedReader.readLine();
                    if (cmd.equals("quit")) {
                        break;
                    }
                    if (cmd.split(" ").length < 2) {
                        System.out.println("wrong command");
                        continue;
                    }
                    if (cmd.split(" ")[0].equals("user")) {
                        service.getUserInfo(Integer.parseInt(cmd.split(" ")[1]));
                        continue;
                    }
                    int driversCount = Integer.parseInt(cmd.split(" ")[0]);
                    int passengersCount = Integer.parseInt(cmd.split(" ")[1]);
                    List<Driver> drivers = UserGenerator.generateDrivers(driversCount);
                    List<Passenger> passengers = UserGenerator.generatePassengers(passengersCount);
                    service.insertDriversToSmartSpace(drivers);
                    service.insertPassengersToSmartSpace(passengers);
                } catch (NumberFormatException e) {
                    System.out.println("wrong command.");
                }
            }
        } catch (IOException ex) {
            System.out.println("Can't read next command. Stopping...");
        } finally {
            service.stop();
        }
        log.log(Level.INFO, "Bye.");
    }
}