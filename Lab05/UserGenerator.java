import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserGenerator {
    public static List<Driver> generateDrivers(int count) {
        List<Driver> ans = new ArrayList<Driver>();
        Random r = new Random();
        for (int i = 0; i < count; i++) {
            ans.add(new Driver(r.nextInt(10000)));
        }
        return ans;
    }

    public static List<Passenger> generatePassengers(int count) {
        List<Passenger> ans = new ArrayList<Passenger>();
        Random r = new Random();
        for (int i = 0; i < count; i++) {
            ans.add(new Passenger(r.nextInt(10000)));
        }
        return ans;
    }
}
