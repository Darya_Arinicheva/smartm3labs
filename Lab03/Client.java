import sofia_kp.KPICore;
import sofia_kp.iKPIC_subscribeHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getAnonymousLogger();
    //private static Logger log = Logger.getLogger(Service.class.getName());

    public Client(String HOST, int PORT, String SMART_SPACE_NAME) {

        log.setLevel(Level.FINER);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINER);
        log.addHandler(handler);
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }

    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }


    public String getStatus(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.queryRDF(subject, "http://workers.ru/RDF/status", null, "uri", "literal");
            Vector<Vector<String>> triple = kpi.xmlTools.getQueryTriple(retXml);
            return (triple.get(0).get(2));
        }
    }

    public String getMoney(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.queryRDF(subject, "http://workers.ru/RDF/money", null, "uri", "literal");
            Vector<Vector<String>> triple = kpi.xmlTools.getQueryTriple(retXml);
            return (triple.get(0).get(2));
        }
    }

    public void start() {
        String retXml = kpi.join();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully joined to smart space.");
        } else {
            log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            return;
        }
    }

    public void stop() {
        // Leave smart space
        String retXml = kpi.leave();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;

        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public static void main(String[] args) throws ParseException {

        Client service = new Client("127.0.0.1", 10010, "X");
        service.start();
        log.info("Started");


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Type 'quit <Enter>' to stop service and leave.\n> ");
        try {
            String cmd = bufferedReader.readLine();
            while (true) {
                if (cmd.equals("exit") || cmd.equals("quit") || cmd.equals("stop")) {
                    break;
                }
                String[] cmds = cmd.split(" ");
                if (cmds[1].equals("insert")) {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM.dd.yyyy");
                        Date begin = simpleDateFormat.parse(cmds[3]);
                        Date end = simpleDateFormat.parse(cmds[4]);
                        String time = String.valueOf(begin.getTime()) + "-" + String.valueOf(end.getTime());
                        service.insert(cmds[0] + "@gmail.com", "http://workers.ru/RDF/" + cmds[2], time, "uri", "literal");
                    } catch (SmartSpaceException e) {
                        e.printStackTrace();
                    }
                } else if (cmds[1].equals("getStatus")) {
                    try {
                        System.out.println(service.getStatus(cmds[0] + "@gmail.com", "http://workers.ru/RDF/status", null, "uri", "literal"));
                    } catch (SmartSpaceException e) {
                        e.printStackTrace();
                    }
                } else if (cmds[1].equals("getMoney")) {
                    try {
                        System.out.println(service.getMoney(cmds[0] + "@gmail.com", "http://workers.ru/RDF/money", null, "uri", "literal"));
                    } catch (SmartSpaceException e) {
                        e.printStackTrace();
                    }
                } else if (!cmds[0].isEmpty()) {
                    System.out.println(String.format("Unknown command: '%s'", cmd));
                }
                System.out.print("> ");
                cmd = bufferedReader.readLine();
            }
        } catch (IOException ex) {
            System.out.println("Can't read next command. Stopping...");
        } finally {
            service.stop();
        }
        log.log(Level.INFO, "Bye.");
    }
}
